_nv_version="r32_release_v3.1"
_bsp_date_ver=32.5.0-20210115145440
_nv_ver_rel="jetson-210_linux_r32.3.1"

pkgbase="nvidia-l4t"
pkgver="32.3.1"
pkgrel="2"
pkgdesc="Tegra BSP drivers"
arch=("aarch64")
url="https://www.nvidia.com/"
license=("GPL")
makedepends=(
	"binutils"
	"zstd"
)
install=tegra-bsp.install

source=(
	# "${_nv_ver_rel}_aarch64.tbz2::https://developer.nvidia.com/embedded/l4t/${_nv_version}/${_nv_version}/t210/${_nv_ver_rel}_aarch64.tbz2"
	"switch-l4t-configs.tar::https://gitlab.com/switchroot/switch-l4t-configs/-/archive/master/switch-l4t-configs.tar"
	"https://developer.nvidia.com/embedded/dlc/r32-3-1_Release_v1.0/t210ref_release_aarch64/Tegra210_Linux_R32.3.1_aarch64.tbz2"
	"https://repo.download.nvidia.com/jetson/common/pool/main/c/cuda/cuda-license-10-2_10.2.89-1_arm64.deb"
	"https://repo.download.nvidia.com/jetson/common/pool/main/c/cuda/cuda-cudart-10-2_10.2.89-1_arm64.deb"
	"https://repo.download.nvidia.com/jetson/common/pool/main/c/cuda/cuda-driver-dev-10-2_10.2.89-1_arm64.deb"
	"https://repo.download.nvidia.com/jetson/common/pool/main/c/cuda/cuda-cudart-dev-10-2_10.2.89-1_arm64.deb"
	"https://repo.download.nvidia.com/jetson/common/pool/main/c/cudnn/libcudnn8_8.0.0.180-1+cuda10.2_arm64.deb"
	"https://repo.download.nvidia.com/jetson/common/pool/main/c/cublas/libcublas10_10.2.2.89-1_arm64.deb"
	"https://repo.download.nvidia.com/jetson/t210/pool/main/t/tensorrt/libnvinfer7_7.1.3-1+cuda10.2_arm64.deb"
	"https://repo.download.nvidia.com/jetson/t210/pool/main/t/tensorrt/libnvinfer-plugin7_7.1.3-1+cuda10.2_arm64.deb"
	"https://repo.download.nvidia.com/jetson/t210/pool/main/t/tensorrt/libnvparsers7_7.1.3-1+cuda10.2_arm64.deb"
	"https://repo.download.nvidia.com/jetson/t210/pool/main/t/tensorrt/libnvonnxparsers7_7.1.3-1+cuda10.2_arm64.deb"
	"https://repo.download.nvidia.com/jetson/t210/pool/main/t/tensorrt/libnvinfer-bin_7.1.3-1+cuda10.2_arm64.deb"
	"https://repo.download.nvidia.com/jetson/common/pool/main/c/cudnn/libcudnn8-dev_8.0.0.180-1+cuda10.2_arm64.deb"
	"https://repo.download.nvidia.com/jetson/common/pool/main/c/cublas/libcublas-dev_10.2.2.89-1_arm64.deb"
	"https://repo.download.nvidia.com/jetson/t210/pool/main/t/tensorrt/libnvinfer-dev_7.1.3-1+cuda10.2_arm64.deb"
	"https://repo.download.nvidia.com/jetson/t210/pool/main/t/tensorrt/libnvinfer-plugin-dev_7.1.3-1+cuda10.2_arm64.deb"
	"https://repo.download.nvidia.com/jetson/t210/pool/main/t/tensorrt/libnvparsers-dev_7.1.3-1+cuda10.2_arm64.deb"
	"https://repo.download.nvidia.com/jetson/t210/pool/main/t/tensorrt/libnvonnxparsers-dev_7.1.3-1+cuda10.2_arm64.deb"
	"https://repo.download.nvidia.com/jetson/t210/pool/main/t/tensorrt/libnvinfer-samples_7.1.3-1+cuda10.2_all.deb"
	"https://repo.download.nvidia.com/jetson/t210/pool/main/t/tensorrt/libnvinfer-doc_7.1.3-1+cuda10.2_all.deb"
	"https://repo.download.nvidia.com/jetson/t210/pool/main/t/tensorrt/tensorrt_7.1.3.0-1+cuda10.2_arm64.deb"
	"https://repo.download.nvidia.com/jetson/t210/pool/main/n/nvidia-l4t-jetson-multimedia-api/nvidia-l4t-jetson-multimedia-api_${_bsp_date_ver}_arm64.deb"
)
md5sums=(
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
	"SKIP"
)

prepare() {
	mv Linux_for_Tegra/nv_tegra/l4t_deb_packages/* .
	tar xf switch-l4t-configs.tar
	mv switch-l4t-configs-* $srcdir/switch-l4t-configs
}

package-3d-core() {
	pkgdesc="Nvidia L4T 3D Core"
	provides=("nvidia-l4t-3d-core")

	ar x nvidia-l4t-3d-core*arm64.deb --output $pkgdir
	tar  -xvf $pkgdir/data.tar.* -C $pkgdir
	rm $pkgdir/data.tar.* $pkgdir/control.tar.* $pkgdir/debian-binary
	echo "/usr/lib/aarch64-linux-gnu/tegra-egl" > $pkgdir/usr/lib/aarch64-linux-gnu/tegra-egl/ld.so.conf
}

package-camera() {
	pkgdesc="Nvidia L4T Camera"
	provides=("nvidia-l4t-camera")

	ar x nvidia-l4t-camera*arm64.deb --output $pkgdir
	tar  -xvf $pkgdir/data.tar.* -C $pkgdir
	rm $pkgdir/data.tar.* $pkgdir/control.tar.* $pkgdir/debian-binary

	mkdir -p $pkgdir/usr/bin
	mv $pkgdir/usr/sbin/* $pkgdir/usr/bin/
	rm -rf $pkgdir/usr/sbin
}

package-configs() {
	pkgdesc="Nvidia L4T Configs"
	provides=("nvidia-l4t-configs")
	
	ar x nvidia-l4t-configs*arm64.deb --output $pkgdir
	tar  -xvf $pkgdir/data.tar.* -C $pkgdir
	rm $pkgdir/data.tar.* $pkgdir/control.tar.* $pkgdir/debian-binary

	cp -r $srcdir/switch-l4t-configs/switch-nvpmodel/* $srcdir
	cp $srcdir/NVIDIA_Login_Logo.png $srcdir/NVIDIA_Logo.png $srcdir/NVIDIA_Wallpaper.jpg \
	  $pkgdir/usr/share/backgrounds/

	cp $srcdir/Linux_for_Tegra/bootloader/nv_boot_control.conf $pkgdir/etc/nv_boot_control.conf
	sed -i 's/boot0/p1/g; s/boot1/p2/g' $pkgdir/etc/nv_boot_control.conf
	
	rm -rf $pkgdir/etc/systemd/system/apt-daily.timer.d/ \
		$pkgdir/etc/systemd/system/apt-daily-upgrade.timer.d/ \
#		$pkgdir/etc/systemd/system/nvfb-early.service \
#		$pkgdir/etc/systemd/system/multi-user.target.wants/nvfb-early.service \
#		$pkgdir/etc/systemd/nvfb-early.sh 

#	rm -rf $pkgdir/etc/xdg/autostart
}

package-core() {
	pkgdesc="Nvidia L4T Core"
	provides=("nvidia-l4t-core")

	ar x nvidia-l4t-core*arm64.deb --output $pkgdir
	tar  -xvf $pkgdir/data.tar.* -C $pkgdir
	rm $pkgdir/data.tar.* $pkgdir/control.tar.* $pkgdir/debian-binary

	echo "/usr/lib/aarch64-linux-gnu" > $pkgdir/etc/ld.so.conf.d/nvidia-tegra.conf
	echo "/usr/lib/aarch64-linux-gnu/tegra" >> $pkgdir/etc/ld.so.conf.d/nvidia-tegra.conf
	echo "/usr/lib/aarch64-linux-gnu/tegra-egl" >> $pkgdir/etc/ld.so.conf.d/nvidia-tegra.conf
	echo "/usr/lib/aarch64-linux-gnu/stubs" >> $pkgdir/etc/ld.so.conf.d/nvidia-tegra.conf
	echo "/usr/lib/aarch64-linux-gnu/libv4l/plugins/nv/" >> $pkgdir/etc/ld.so.conf.d/nvidia-tegra.conf 
	echo "/usr/lib/aarch64-linux-gnu/tegra/weston/" >> $pkgdir/etc/ld.so.conf.d/nvidia-tegra.conf
	echo "/usr/lib/aarch64-linux-gnu/weston/" >> $pkgdir/etc/ld.so.conf.d/nvidia-tegra.conf
	echo "/usr/lib/weston/" >> $pkgdir/etc/ld.so.conf.d/nvidia-tegra.conf
	echo "/usr/lib/aarch64-linux-gnu/gstreamer-1.0/" >> $pkgdir/etc/ld.so.conf.d/nvidia-tegra.conf
	echo "/usr/local/cuda-10.2/lib64/" >> $pkgdir/etc/ld.so.conf.d/nvidia-tegra.conf

	mkdir -p $pkgdir/usr/bin
	mv $pkgdir/usr/sbin/* $pkgdir/usr/bin/
	rm -rf $pkgdir/usr/sbin
}

package-firmware() {
	pkgdesc="Nvidia L4T Firmware files"
	provides=("nvidia-l4t-firmware")

	ar x nvidia-l4t-firmware*arm64.deb --output $pkgdir
	tar  -xvf $pkgdir/data.tar.* -C $pkgdir
	rm $pkgdir/data.tar.* $pkgdir/control.tar.* $pkgdir/debian-binary

	mkdir -p $pkgdir/usr/bin $pkgdir/usr/lib/
	mv $pkgdir/usr/sbin/* $pkgdir/usr/bin/
	mv $pkgdir/lib/* $pkgdir/usr/lib/
	rm -rf $pkgdir/usr/sbin $pkgdir/lib

	pushd $pkgdir/usr/lib/firmware/gm20b > /dev/null 2>&1
		ln -sf "../tegra21x/acr_ucode.bin" "acr_ucode.bin"
 		ln -sf "../tegra21x/gpmu_ucode.bin" "gpmu_ucode.bin"
		ln -sf "../tegra21x/gpmu_ucode_desc.bin" "gpmu_ucode_desc.bin"
		ln -sf "../tegra21x/gpmu_ucode_image.bin" "gpmu_ucode_image.bin"
		ln -sf "../tegra21x/gpu2cde.bin" "gpu2cde.bin"
		ln -sf "../tegra21x/NETB_img.bin" "NETB_img.bin"
		ln -sf "../tegra21x/fecs_sig.bin" "fecs_sig.bin"
		ln -sf "../tegra21x/pmu_sig.bin" "pmu_sig.bin"
		ln -sf "../tegra21x/pmu_bl.bin" "pmu_bl.bin"
		ln -sf "../tegra21x/fecs.bin" "fecs.bin"
 		ln -sf "../tegra21x/gpccs.bin" "gpccs.bin"
	popd > /dev/null

	rm $pkgdir/usr/lib/systemd/system/bluetooth.service.d/nv-bluetooth-service.conf
}

package-graphics-demos() {
	pkgdesc="Nvidia L4T graphics demos"
	provides=("nvidia-l4t-graphics-demos")
	ar x nvidia-l4t-graphics-demos*arm64.deb --output $pkgdir
	tar  -xvf $pkgdir/data.tar.* -C $pkgdir
	rm $pkgdir/data.tar.* $pkgdir/control.tar.* $pkgdir/debian-binary
}

package-gstreamer() {
	pkgdesc="Nvidia L4T Gstreamer"
	provides=("nvidia-l4t-gstreamer")

	ar x nvidia-l4t-gstreamer*arm64.deb --output $pkgdir
	tar  -xvf $pkgdir/data.tar.* -C $pkgdir
	for link in `find $pkgdir/usr/lib/aarch64-linux-gnu/ -maxdepth 1 -type f -printf '%f\n'`; do
		ln -sfn /usr/lib/aarch64-linux-gnu/$link $pkgdir/usr/lib/$link
	done
	cp -r $pkgdir/usr/lib/aarch64-linux-gnu/gstreamer-1.0/ $pkgdir/usr/lib/
   	rm $pkgdir/data.tar.* $pkgdir/control.tar.* $pkgdir/debian-binary
}

package-init() {
	pkgdesc="Nvidia L4T init"
	provides=("nvidia-l4t-init")

	ar x nvidia-l4t-init_*_arm64.deb --output $pkgdir
	tar  -xvf $pkgdir/data.tar.* -C $pkgdir
	rm $pkgdir/data.tar.* $pkgdir/control.tar.* $pkgdir/debian-binary

	rm -rf $pkgdir/etc/wpa_supplicant.conf \
		$pkgdir/etc/systemd/system/nv-l4t-usb-device-mode-runtime.service \
		$pkgdir/etc/systemd/system/nv-l4t-usb-device-mode.service \
		$pkgdir/etc/systemd/system/multi-user.target.wants/nv-l4t-usb-device-mode.service \
		$pkgdir/etc/systemd/sleep.conf
#		$pkgdir/opt/
#		$pkgdir/etc/systemd/nvfb.sh \
#		$pkgdir/etc/systemd/nvgetty.sh \
#		$pkgdir/etc/systemd/nvmemwarning.sh \
#		$pkgdir/etc/systemd/system/nvfb.service \
#		$pkgdir/etc/systemd/system/nvmemwarning.service \
#		$pkgdir/etc/systemd/system/nvzramconfig.service \
#		$pkgdir/etc/systemd/system/nvgetty.service \
#		$pkgdir/etc/systemd/system/nvphs.service \
#		$pkgdir/etc/systemd/system/nvs-service.service \
#		$pkgdir/etc/systemd/system/getty.target.wants \
#		$pkgdir/etc/systemd/nvzramconfig.sh \
#	    $pkgdir/etc/systemd/system/nvfb-early.service \
#		$pkgdir/etc/systemd/system/multi-user.target.wants/nvfb.service \
#		$pkgdir/etc/systemd/system/multi-user.target.wants/nvgetty.service \
#		$pkgdir/etc/systemd/system/multi-user.target.wants/nv-l4t-bootloader-config.service \
#		$pkgdir/etc/systemd/system/multi-user.target.wants/nvmemwarning.service \
#		$pkgdir/etc/systemd/system/multi-user.target.wants/nvphs.service \
#		$pkgdir/etc/systemd/system/multi-user.target.wants/nvzramconfig.service \

	mkdir -p $pkgdir/usr/bin
	mv $pkgdir/usr/sbin/* $pkgdir/usr/bin/
	rm -rf $pkgdir/usr/sbin
}

package-initrd() {
    pkgdesc="Nvidia L4T initrd"
    provides=("nvidia-l4t-initrd")

    ar x nvidia-l4t-initrd_*_arm64.deb --output $pkgdir
    tar  -xvf $pkgdir/data.tar.* -C $pkgdir
    rm $pkgdir/data.tar.* $pkgdir/control.tar.* $pkgdir/debian-binary
}

package-jetson-dtbs() {
	mkdir $pkgdir/boot/
	cp Linux_for_Tegra/kernel/dtb/*.dtb* $pkgdir/boot/
}

package-jetson-io() {
	pkgdesc="Nvidia L4T Jetson-IO"
	provides=("nvidia-l4t-jetson-io")

	ar x nvidia-l4t-jetson-io*arm64.deb --output $pkgdir
	tar  -xvf $pkgdir/data.tar.* -C $pkgdir
   	rm $pkgdir/data.tar.* $pkgdir/control.tar.* $pkgdir/debian-binary
}

package-jetson-multimedia-api() {
	ar x nvidia-l4t-jetson-multimedia-api*arm64.deb --output $pkgdir
	tar  -xvf $pkgdir/data.tar.* -C $pkgdir
	rm $pkgdir/data.tar.* $pkgdir/control.tar.* $pkgdir/debian-binary
}

package-multimedia() {
	pkgdesc="Nvidia L4T Multimedia"
	provides=("nvidia-l4t-multimedia")

	ar x nvidia-l4t-multimedia_*_arm64.deb --output $pkgdir
	tar  -xvf $pkgdir/data.tar.* -C $pkgdir
	rm $pkgdir/data.tar.* $pkgdir/control.tar.* $pkgdir/debian-binary
}

package-multimedia-utils() {
	pkgdesc="Nvidia L4T Multimedia Utils"
	provides=("nvidia-l4t-multimedia-utils")

	ar x nvidia-l4t-multimedia-utils*arm64.deb --output $pkgdir
	tar  -xvf $pkgdir/data.tar.* -C $pkgdir
	rm $pkgdir/data.tar.* $pkgdir/control.tar.* $pkgdir/debian-binary
}

package-oem-config() {
	pkgdesc="Nvidia L4T OEM config"
	provides=("nvidia-l4t-oem-config")

	ar x nvidia-l4t-oem-config*arm64.deb --output $pkgdir
	tar  -xvf $pkgdir/data.tar.* -C $pkgdir
	rm $pkgdir/data.tar.* $pkgdir/control.tar.* $pkgdir/debian-binary 

	rm -rf  $pkgdir/usr/lib/ubiquity/ \
#		$pkgdir/etc/nv-oem-config.conf \
#		$pkgdir/etc/systemd/nv-oem-config-post.sh \
#		$pkgdir/etc/systemd/nv-oem-config.sh \
#		$pkgdir/usr/lib/systemd/system/nv-oem-config-debconf@.service \
#		$pkgdir/usr/lib/systemd/system/nv-oem-config-gui.service \
#		$pkgdir/usr/lib/systemd/system/nv-oem-config.service \
#		$pkgdir/usr/lib/systemd/system/nv-oem-config.target \
#		$pkgdir/usr/bin/nv-oem-config-firstboot \
#		$pkgdir/usr/lib/nvidia/qspi-update \
#		$pkgdir/usr/lib/nvidia/swap/ \
#		$pkgdir/usr/lib/nvidia/resizefs/

	mkdir -p $pkgdir/usr/bin $pkgdir/usr/lib/
	mv $pkgdir/usr/sbin/* $pkgdir/usr/bin/
	mv $pkgdir/lib/* $pkgdir/usr/lib/
	rm -rf $pkgdir/usr/sbin $pkgdir/lib
}

package-tools() {
	pkgdesc="Nvidia L4T Tools"
	provides=("nvidia-l4t-tools")

	ar x nvidia-l4t-tools*arm64.deb --output $pkgdir
	tar  -xvf $pkgdir/data.tar.* -C $pkgdir
	rm $pkgdir/data.tar.* $pkgdir/control.tar.* $pkgdir/debian-binary

	cp -r $srcdir/switch-l4t-configs/switch-nvpmodel/* $srcdir
  
	chmod +x $srcdir/nvpmodel_helper.sh
	cp $srcdir/nv_logo.svg $srcdir/nvpmodel_helper.sh $srcdir/nvpmodel_indicator.py \
	  $srcdir/nvpmodel.py $pkgdir/usr/share/nvpmodel_indicator/
 
	cp $srcdir/nvpmodel_t210.conf $pkgdir/etc/nvpmodel/

	sed -i s/x-terminal-emulator/konsole/g $pkgdir/usr/share/nvpmodel_indicator/nvpmodel_indicator.py
	sed -i 's/OnlyShowIn.*//g' $pkgdir/etc/xdg/autostart/nvpmodel_indicator.desktop

	mkdir -p $pkgdir/usr/bin
	mv $pkgdir/usr/sbin/* $pkgdir/usr/bin/
	rm -rf $pkgdir/usr/sbin
}

package-wayland() {
	pkgdesc="Nvidia L4T Wayland"
	provides=("nvidia-l4t-wayland")

	ar x nvidia-l4t-wayland*arm64.deb --output $pkgdir
	tar  -xvf $pkgdir/data.tar.* -C $pkgdir
	rm $pkgdir/data.tar.* $pkgdir/control.tar.* $pkgdir/debian-binary
}

package-weston() {
	pkgdesc="Nvidia L4T Weston"
	provides=("nvidia-l4t-weston")

	ar x nvidia-l4t-weston*arm64.deb --output $pkgdir
	tar  -xvf $pkgdir/data.tar.* -C $pkgdir
	rm $pkgdir/data.tar.* $pkgdir/control.tar.* $pkgdir/debian-binary
}

package-x11() {
	pkgdesc="Nvidia L4T X.Org configuration files"
	provides=("nvidia-l4t-x11")

	ar x nvidia-l4t-x11*arm64.deb --output $pkgdir
	tar  -xvf $pkgdir/data.tar.* -C $pkgdir
	rm $pkgdir/data.tar.* $pkgdir/control.tar.* $pkgdir/debian-binary
}

package-xusb-firmware() {
	pkgdesc="Nvidia L4T XUSB firmware"
	provides=("nvidia-l4t-xusb-firmware")

	ar x nvidia-l4t-xusb-firmware*arm64.deb --output $pkgdir
	tar  -xvf $pkgdir/data.tar.* -C $pkgdir
	rm $pkgdir/data.tar.* $pkgdir/control.tar.* $pkgdir/debian-binary

	mkdir -p $pkgdir/usr/lib/
	mv $pkgdir/lib/* $pkgdir/usr/lib/
	rm -rf $pkgdir/lib
}

#rm -rf $pkgdir/etc/hosts $pkgdir/etc/hostname $pkgdir/etc/fstab

pkgname=(
	"${pkgbase}-3d-core"
	"${pkgbase}-camera"
	"${pkgbase}-configs"
	"${pkgbase}-core"
	"${pkgbase}-firmware"
	"${pkgbase}-graphics-demos"
	"${pkgbase}-gstreamer"
	"${pkgbase}-init"
	"${pkgbase}-initrd"
	"${pkgbase}-jetson-io"
	"${pkgbase}-jetson-dtbs"
	"${pkgbase}-jetson-multimedia-api"
	"${pkgbase}-multimedia"
	"${pkgbase}-multimedia-utils"
	"${pkgbase}-oem-config"
	"${pkgbase}-tools"
	"${pkgbase}-wayland"
	"${pkgbase}-weston"
	"${pkgbase}-x11"
	"${pkgbase}-xusb-firmware"
)
for _p in "${pkgname[@]}"; do
  eval "package_$_p() {
    $(declare -f "package${_p#$pkgbase}")
    package${_p#$pkgbase}
  }"
done

