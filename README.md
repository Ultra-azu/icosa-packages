# L4T-Packages-Repository

Packages recipes for L4T distribution.

## Packages avalaible

`linux-tegra` : Linux 4 Tegra - 4.9. \
`nvidia-drivers-package` : Tegra BSP drivers. \
`switch-boot-files-bin` : Switch boot files. \
`switch-configs` : Switch specific peripheral confgiurations (ALSA, Dock ...). \
`tegra-ffmpeg` : FFmpeg 4.2 patched for jetson boards. \
`jetson-ffmpeg` : Compatibility layer for jetson board and ffmpeg.

## Building packages

**Note:** We are going to use `switch-configs` package throughout the example, tweak accordingly to the package you want to rebuild.

```sh
docker run --rm -it -v `pwd`/pkgbuilds/switch-configs:/pkg/ -v /var/run/docker.sock:/var/run/docker.sock -e SHARED_VOLUME=$(pwd)/pkgbuilds/switch-configs l4t_packages/arch
```
